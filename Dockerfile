# Usar una imagen base de Node.js
FROM node:14

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /usr/src/app

# Copiar los archivos de tu proyecto al directorio de trabajo en el contenedor
COPY . .


# Instalar las dependencias
RUN npm install

# Exponer el puerto en el que se ejecuta tu aplicación
EXPOSE 3000

# Comando para iniciar tu aplicación cuando se ejecute el contenedor
CMD [ "node", "app.js" ]