var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require("./routes/routes.js")(app);

app.use((req, res, next) => {
    res.status(404).json({ error: 'Ruta no encontrada' });
});

var server = app.listen(3000, function () {
    console.log("Listening on port %s...", server.address().port);
});
