const { fibonacci, validate } = require("./utils/fibonacci");

var appRouter = function (app) {
  app.get("/fibonacci/:number", function (req, res) {

    const number = parseInt(req.params.number);

	console.log(number)

    const validateNumber = validate(number);
    if (!validateNumber)
      return res.send({ status: "error", message: "number parameter error" });

    const result = fibonacci(number);
    return res.send({ status: "success", result: result });
  });
};

module.exports = appRouter;
