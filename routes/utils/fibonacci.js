function fibonacci(num) {
  if (num == 0) return 0;
  if (num == 1) return 1;

  //console.log(num - 2, "+", num - 1);
  return  fibonacci(num - 1) + fibonacci(num - 2);
}

function validate(val) {
  if (isNaN(val) || val < 0) {
    return false;
  }
  return true;
}

module.exports = {
  fibonacci,
  validate,
};
