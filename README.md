La API REST de Fibonacci tiene una sola característica:

- Dado un índice `n`, la API responderá con el valor de la posición `n` de la serie de Fibonacci. Ejemplo: si `n=3` 
   la respuesta debe ser `2`, si `n=6` la respuesta debe ser `8`.

Para correr el proyecto debe seguir estos pasos:
1 npm install
2 npm start
3 el proyecto se ejecutara en el puerto 3000 ejemplo: http://localhost:3000/fibonacci/6
4 donde 6 es el indice que recibe el api como parametro


Para correrlo en docker:

1  build -t mi-restapi-fibonacci .
2  docker run -p 3000:3000 mi-app-fibonacci 



url gitlab: https://gitlab.com/cristian.ballon/fibonacci-restapi